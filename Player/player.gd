extends CharacterBody2D

@export var MAX_SPEED = 1200
@export var ACCELERATION = 5000
@export var FRICTION = 3000
@export var jumps = 2
@onready var axis = Vector2.ZERO

const JUMP_VELOCITY = -1200.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = 3000

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("move_up") and jumps > 0:
		velocity.y = JUMP_VELOCITY
		jumps -= 1
		
	if is_on_floor() and jumps < 2:
		jumps += 1
	move(delta)
	
	
func get_input_axis():
	axis.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	return axis.normalized()

func move(delta):
	axis = get_input_axis()
	
	if axis.x == 0:
		apply_friction(FRICTION * delta)
	else:
		apply_movement(axis.x * ACCELERATION * delta)
		
	move_and_slide()
	
	

func apply_friction(deacceleration):
	if abs(velocity.x) > deacceleration:
		print("normalized velocity: ", velocity.normalized())
		print("deacceleration: ", deacceleration)
		if velocity.x > 0:
			velocity.x -= deacceleration
		elif velocity.x < 0:
			velocity.x += deacceleration
		# velocity.x += -velocity.normalized().x * deacceleration
	else:
		velocity.x = 0
		

func apply_movement(acceleration):
	if abs(velocity.x) < MAX_SPEED:
		velocity.x += acceleration


'const SPEED = 300.0
const JUMP_VELOCITY = -400.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")


func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("ui_left", "ui_right")
	if direction:
		velocity.x = direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)

	move_and_slide()
'
