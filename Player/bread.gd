extends CharacterBody2D

var speed = 1000
var acceleration_factor = 0.2
var input_vector = Vector2()
var time_held = 1.0
var inertia_damping = 0.2

func _ready():
	#screen_size = get_viewport().get_visible_rect().size
	pass
	
func _physics_process(delta):
	input_vector = Vector2()

	# Get input
	if Input.is_action_pressed("ui_right"):
		input_vector.x += 1
	if Input.is_action_pressed("ui_left"):
		input_vector.x -= 1
	if Input.is_action_pressed("ui_down"):
		input_vector.y += 1
	if Input.is_action_pressed("ui_up"):
		input_vector.y -= 1

	input_vector = input_vector.normalized()

	# Update time held for acceleration
	if input_vector != Vector2():
		time_held += delta
	else:
		time_held = 0

	apply_movement_to_body(input_vector, delta)
	move_and_slide()

func apply_movement_to_body(direction: Vector2, delta: float):
	var target_velocity = direction * speed

	# Check if input is opposite to current direction (for removing inertia)
	if direction.dot(self.velocity.normalized()) < -0.5:
		self.velocity = Vector2.ZERO

	# Apply inertia when changing directions
	self.velocity = self.velocity.lerp(target_velocity, inertia_damping + delta)


func _on_area_2d_area_entered(area):
	if area.is_in_group('platform'):
		queue_free()
