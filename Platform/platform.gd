extends StaticBody2D

@export var width = 200
@export var direction = Vector2(100,100)
@export var speed = 200
@export var rotation_speed = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.rotation += rotation_speed
	translate(direction * speed * delta)
	
	
	
	#constant_linear_velocity = Vector2(-speed, 0) 
