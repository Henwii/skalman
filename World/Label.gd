extends Label

var timer
var timer_label

func _ready():
	# Create a Timer
	timer = Timer.new()
	timer.set_wait_time(1)  # Set the timer interval to 1 second
	timer.set_one_shot(false)  # Make the timer repeat
	
	# Connect the timer timeout signal to the _on_timer_timeout function
	timer.connect("timeout", self, "_on_tim_timeout")
	
	# Add the timer to the scene
	add_child(timer)
	
	# Create a Label to display the timer
	timer_label = Label.new()
	timer_label.rect_min_size = Vector2(200, 50)  # Set the size of the label
	timer_label.align = Label.PRESET_CENTER  # Center-align the text
	add_child(timer_label)

	# Start the timer
	timer.start()

# This function is called every time the timer times out
func _on_timer_timeout():
	# Update the label text with the elapsed time
	timer_label.text = "Time: " + str(timer.get_time_left()) + "s"

func _process(delta):
	# Check for game end condition (replace this with your own logic)
	if game_has_ended():
		# Stop the timer when the game ends
		timer.stop()
		timer_label.text = "Game Over! Time: " + str(timer.get_time_left()) + "s"

# Replace this function with your own game end condition logic
func game_has_ended() -> bool:
	return false
