extends Timer

func _ready():
	# Connect the timeout signal to the _on_timer_timeout function
	connect("timeout", self, "_on_timer_timeout")
	
	# Set the wait time for the timer (10 seconds)
	wait_time = 10
	set_wait_time(wait_time)
	
	# Start the timer
	start()

func _on_timer_timeout():
	# This function will be called every 10 seconds
	print("Function executed!")
	# Replace the print statement with the actual code you want to run
