extends Node2D
@onready var packedPlatform = preload("res://Platform/platform.tscn")
@onready var Pause_Menu = $PauseMenu

var paused = false

var spawn_timer = Timer.new()
var knife_count = 10
var increase_rate = 60.0
var time_passed = 0.0

@onready var packedBread = preload("res://Player/bread.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	add_child(spawn_timer)
	spawn_timer.wait_time = 5	
	spawn_timer.start()
	spawn_knives()	
	spawn_timer.timeout.connect(spawn_knives)
	spawn_timer.wait_time = 10	
	
	
func spawn_knives():
	
	for i in range(1,knife_count):
		var middle_x = get_random_number_between(0,1920)
		var middle_y = get_random_number_between(0,1080)
		
		var spawn_angle = randf_range(0, 2 * PI)
		var spawn_distance_from_center = get_random_number_between(1200,4000)
		randomize()
		
		var platform_spawn_x = 1920/2 + spawn_distance_from_center * cos(spawn_angle)
		var platform_spawn_y = 1080/2 + spawn_distance_from_center * sin(spawn_angle)
		
		var x_diff = middle_x-platform_spawn_x
		var y_diff = middle_y-platform_spawn_y
		var angle = atan2(y_diff, x_diff)
		var direction = Vector2(x_diff, y_diff)
		
		var platform = packedPlatform.instantiate()
		platform.position = Vector2(platform_spawn_x, platform_spawn_y)
		platform.scale = Vector2(1,1) * get_random_number_between(1,3)
		platform.speed = get_random_number_between(300, 400)
		platform.direction = Vector2(x_diff,y_diff).normalized()
		platform.rotation = angle
		platform.rotation_speed = get_random_number_between(-1,0)
		add_child(platform)
	knife_count += 5

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#pause menu
	if Input.is_action_just_pressed("pause"):
		PauseMenu()
		
func PauseMenu():
	if paused:
		Pause_Menu.hide()
		Engine.time_scale = 1
	else:
		Pause_Menu.show()
		Engine.time_scale = 0

	paused = !paused

func get_random_number_between(start, end):
	randomize()
	return randi_range(start,end)
