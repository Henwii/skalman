extends Control

func set_score(value):
	$Panel/Score.text "Score: " + str(value)

func set_Highscore(value):
	$Panel/HighScore.text "Score: " + str(value)

func _on_restart_pressed():
	get_tree().change_scene_to_file("res://World/world.tscn")

func _on_ragequit_pressed():
	get_tree(). quit()
